package com.sibs.agent.agent;

import com.sibs.agent.dto.*;
import com.sibs.agent.service.PrtService;
import com.sibs.agent.utility.SibsLog;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.SNMP4JSettings;
import org.snmp4j.TransportMapping;
import org.snmp4j.agent.*;
import org.snmp4j.agent.mo.MOAccessImpl;
import org.snmp4j.agent.mo.MOScalar;
import org.snmp4j.agent.mo.snmp.*;
import org.snmp4j.agent.security.MutableVACM;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.MessageProcessingModel;
import org.snmp4j.security.*;
import org.snmp4j.smi.*;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.TransportMappings;
import org.snmp4j.util.ThreadPool;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class AgentV3 extends BaseAgent {
    protected String address;
    private Credentials credentials = new Credentials();
    private Config agentConfig = new Config();
    private final OctetString contextName = new OctetString();
    private final SibsLog sibsLog = new SibsLog();

    static {
        //LogFactory.setLogFactory( new ConsoleLogFactory() );
        //ConsoleLogAdapter.setDebugEnabled(true);

    }

    public AgentV3(CommandProcessor processor) {
        super(new File("SNMP4JTestAgentBC.cfg"), new File("SNMP4JTestAgentConfig.cfg"),
                processor);
        agent.setWorkerPool(ThreadPool.create("RequestPool", 4));
        SNMP4JSettings.setForwardRuntimeExceptions(true);
        SNMP4JSettings.setReportSecurityLevelStrategy(SNMP4JSettings.ReportSecurityLevelStrategy.noAuthNoPrivIfNeeded);

    }

    protected void registerManagedObjects() {
        // getSnmpv2MIB().unregisterMOs(this.server, getContext(getSnmpv2MIB()));

        ManagedObject[] objects = new ManagedObject[3];
        objects[0] = new MOScalar(new OID( Mib.MibOid.STATUSPRT.toString() ), MOAccessImpl.ACCESS_READ_ONLY, new OctetString(""));
        objects[1] = new MOScalar(new OID( Mib.MibOid.TOTALSESSION.toString() ), MOAccessImpl.ACCESS_READ_ONLY, new OctetString(""));
        objects[2] = new MOScalar(new OID( Mib.MibOid.MESSAGE.toString() ), MOAccessImpl.ACCESS_READ_ONLY, new OctetString(""));

        for (ManagedObject object : objects) {
            try {
                this.server.register(object, null);
                sibsLog.log(SibsLog.SibsErroStatus.ALL, "#Create MIB OID:{0}#", new Object[]{object});
                server.addLookupListener(new MOServerLookupListener() {
                    @Override
                    public void lookupEvent(MOServerLookupEvent event) {
                        ((MOScalar) event.getLookupResult()).setValue(getValuePrt(((MOScalar<?>) event.getLookupResult()).getOid()));
                    }

                    @Override
                    public void queryEvent(MOServerLookupEvent event) {
                        //
                    }
                }, object);

            } catch (DuplicateRegistrationException e) {
                throw new RuntimeException(e);
            }
        }

    }

    private OctetString getValuePrt(OID oid) {

        PrtService prtService = new PrtService();
        List<Session> sessionList = prtService.getSessions(agentConfig.getPrtServer().getHost(), agentConfig.getPrtServer().getPort());
        OctetString result = null;

        if (sessionList.isEmpty()) {
            return new OctetString("PRT DOWN");
        }

        switch (Mib.MibOid.getEnum(String.valueOf(oid))) {

            case STATUSPRT:
                result = new OctetString("PRT UP");
                break;
            case TOTALSESSION:
                result = new OctetString(String.valueOf(sessionList.size()));
                break;
            case MESSAGE:
                result = new OctetString(getAllSessionData(sessionList));
                break;
        }

        return result;
    }

    private String getAllSessionData(List<Session> sessionList) {

        String returnValues = "";

        for (Session session : sessionList) {
            returnValues += "numeroSessao=" + session.getPrtses() + "&estadoSessao=" + session.getSta() + "&estadoTrafego=" + session.getTrf() + "&estadoTransport=" + session.getTrp() + "&numeroTotalIlt=" +
                    session.getIlts() + "&numeroIltOcupados=" + (session.getIlts() - session.getFre()) + "&numeroIltLivres=" + session.getFre() + "&numeroIltRepeticoes=" + session.getRty() + "&";
        }

        return returnValues;
    }

    public void sendnotification(OctetString context, OID notificationID, VariableBinding[] vbs) {
        sibsLog.log(SibsLog.SibsErroStatus.ALL, "#BEGIN Send notification:{0}", new Object[]{notificationID});
        this.notificationOriginator.notify(context, notificationID, vbs);
        sibsLog.log(SibsLog.SibsErroStatus.ALL, "#END Send notification:{0}", new Object[]{notificationID});
    }

    @Override
    protected void addNotificationTargets(SnmpTargetMIB targetMIB, SnmpNotificationMIB notificationMIB) {

        targetMIB.addDefaultTDomains();


        for (Traps traps : agentConfig.getTrapsConfig()) {
            targetMIB.addTargetAddress(new OctetString("notificationV3"),
                    TransportDomains.transportDomainUdpIpv4,
                    new OctetString(new UdpAddress(traps.getHost() + "/" + traps.getPort()).getValue()),
                    200, 1,
                    new OctetString("notify"),
                    new OctetString("v3notify"),
                    StorageType.nonVolatile);

            targetMIB.addTargetParams(new OctetString("v3notify"),
                    MessageProcessingModel.MPv3,
                    SecurityModel.SECURITY_MODEL_USM,
                    new OctetString("v3notify"),
                    SecurityLevel.AUTH_PRIV,
                    StorageType.nonVolatile);

            notificationMIB.addNotifyEntry(new OctetString("default"),
                    new OctetString("notify"),
                    SnmpNotificationMIB.SnmpNotifyTypeEnum.trap,
                    StorageType.nonVolatile);
        }

    }

    @Override
    protected void addViews(VacmMIB vacmMIB) {
        // SNMPv3
        for (Credential credentials : credentials.getCredentials()) {

            switch (credentials.getTypeUserName()) {

                case "GET":

                    vacmMIB.addGroup(
                            SecurityModel.SECURITY_MODEL_USM,
                            new OctetString(credentials.getUserName()),
                            new OctetString("v3group"),
                            StorageType.nonVolatile
                    );

                    vacmMIB.addAccess(
                            new OctetString("v3group"),
                            new OctetString(this.contextName),
                            SecurityModel.SECURITY_MODEL_USM,
                            SecurityLevel.AUTH_PRIV,
                            MutableVACM.VACM_MATCH_EXACT,
                            new OctetString("fullReadView"),
                            new OctetString("fullWriteView"),
                            new OctetString("fullNotifyView"),
                            StorageType.volatile_
                    );

                    break;
                case "NOTIFICATION":

                    vacmMIB.addGroup(
                            SecurityModel.SECURITY_MODEL_USM,
                            new OctetString(credentials.getUserName()),
                            new OctetString("v3notify"),
                            StorageType.volatile_
                    );

                    vacmMIB.addAccess(
                            new OctetString("v3notify"),
                            new OctetString(this.contextName),
                            SecurityModel.SECURITY_MODEL_USM,
                            SecurityLevel.NOAUTH_NOPRIV,
                            MutableVACM.VACM_MATCH_EXACT,
                            new OctetString("fullReadView"),
                            new OctetString("fullWriteView"),
                            new OctetString("fullNotifyView"),
                            StorageType.volatile_
                    );
                    break;
            }

        }

        vacmMIB.addViewTreeFamily(new OctetString("fullReadView"), new OID("1.3"),
                new OctetString(), VacmMIB.vacmViewIncluded,
                StorageType.nonVolatile);

        vacmMIB.addViewTreeFamily(new OctetString("fullWriteView"), new OID("1.3"),
                new OctetString(), VacmMIB.vacmViewIncluded,
                StorageType.nonVolatile);

        vacmMIB.addViewTreeFamily(new OctetString("fullNotifyView"), new OID("1.3"),
                new OctetString(), VacmMIB.vacmViewIncluded,
                StorageType.nonVolatile);

    }

    @Override
    protected void addUsmUser(USM usm) {

        for (Credential credentials : credentials.getCredentials()) {
            UsmUser user = new UsmUser(new OctetString(credentials.getUserName()),
                    AuthSHA.ID,
                    new OctetString(credentials.getAuthPassword()),
                    //PrivAES256.ID,
                    PrivAES128.ID,
                    new OctetString(credentials.getPrivPassword()));

            usm.addUser(user.getSecurityName(), user.getLocalizationEngineID(), user);

        }
    }

    protected void initTransportMappings() throws IOException {
        transportMappings = new TransportMapping[2];
        Address addr = GenericAddress.parse(address);
        TransportMapping tm =
                TransportMappings.getInstance().createTransportMapping(addr);
        transportMappings[0] = tm;
        transportMappings[1] = new DefaultTcpTransportMapping(new TcpAddress(address));
    }

    public void start(String ip, int port) throws IOException {
        sibsLog.log(SibsLog.SibsErroStatus.ALL, "#Begin start Agente");
        address = ip + "/" + port;
        //BasicConfigurator.configure();
        init();
        addShutdownHook();
        //loadConfig(ImportModes.REPLACE_CREATE);
        getServer().addContext(new OctetString());
        finishInit();
        run();
        sendColdStartNotification();
        sibsLog.log(SibsLog.SibsErroStatus.ALL, "#End start Agente");
    }

    protected void unregisterManagedObjects() {

    }

    protected void addCommunities(SnmpCommunityMIB communityMIB) {

    }

    protected void registerSnmpMIBs() {
        super.registerSnmpMIBs();
    }

    protected void initMessageDispatcher() {

        this.dispatcher = new MessageDispatcherImpl();
        this.mpv3 = new MPv3(this.agent.getContextEngineID().getValue());
        this.usm = new USM(SecurityProtocols.getInstance(), this.agent.getContextEngineID(), this.updateEngineBoots());
        SecurityModels.getInstance().addSecurityModel(this.usm);
        SecurityProtocols.getInstance().addDefaultProtocols();
        this.dispatcher.addMessageProcessingModel(this.mpv3);
        this.initSnmpSession();

    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public Config getAgentConfig() {
        return agentConfig;
    }

    public void setAgentConfig(Config agentConfig) {
        this.agentConfig = agentConfig;
    }
}