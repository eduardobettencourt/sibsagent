package com.sibs.agent.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Response
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-01-24T16:15:24.740Z[GMT]")


public class Credentials {
    @JsonProperty("description")
    private String description = null;

    @JsonProperty("credentials")
    @Valid
    private List<Credential> credentials = null;

    public Credentials description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Get description
     *
     * @return description
     **/
    @Schema(description = "")

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Credentials credentials(List<Credential> credentials) {
        this.credentials = credentials;
        return this;
    }

    public Credentials addCredentialsItem(Credential credentialsItem) {
        if (this.credentials == null) {
            this.credentials = new ArrayList<Credential>();
        }
        this.credentials.add(credentialsItem);
        return this;
    }

    /**
     * Get credentials
     *
     * @return credentials
     **/
    @Schema(description = "")
    @Valid
    public List<Credential> getCredentials() {
        return credentials;
    }

    public void setCredentials(List<Credential> credentials) {
        this.credentials = credentials;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Credentials response = (Credentials) o;
        return Objects.equals(this.description, response.description) &&
                Objects.equals(this.credentials, response.credentials) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, credentials);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Response {\n");

        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    credentials: ").append(toIndentedString(credentials)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
