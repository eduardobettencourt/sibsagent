package com.sibs.agent.dto;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Credential
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-01-24T16:15:24.740Z[GMT]")


public class Credential   {
  @JsonProperty("userName")
  private String userName = null;

  @JsonProperty("typeUserName")
  private String typeUserName = null;

  @JsonProperty("authPassword")
  private String authPassword = null;

  @JsonProperty("privPassword")
  private String privPassword = null;

  public Credential userName(String userName) {
    this.userName = userName;
    return this;
  }

  /**
   * Get userName
   * @return userName
   **/
  @Schema(description = "")
  
    public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Credential typeUserName(String typeUserName) {
    this.typeUserName = typeUserName;
    return this;
  }

  /**
   * Get typeUserName
   * @return typeUserName
   **/
  @Schema(description = "")
  
    public String getTypeUserName() {
    return typeUserName;
  }

  public void setTypeUserName(String typeUserName) {
    this.typeUserName = typeUserName;
  }

  public Credential authPassword(String authPassword) {
    this.authPassword = authPassword;
    return this;
  }

  /**
   * Get authPassword
   * @return authPassword
   **/
  @Schema(description = "")
  
    public String getAuthPassword() {
    return authPassword;
  }

  public void setAuthPassword(String authPassword) {
    this.authPassword = authPassword;
  }

  public Credential privPassword(String privPassword) {
    this.privPassword = privPassword;
    return this;
  }

  /**
   * Get privPassword
   * @return privPassword
   **/
  @Schema(description = "")
  
    public String getPrivPassword() {
    return privPassword;
  }

  public void setPrivPassword(String privPassword) {
    this.privPassword = privPassword;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Credential credential = (Credential) o;
    return Objects.equals(this.userName, credential.userName) &&
        Objects.equals(this.typeUserName, credential.typeUserName) &&
        Objects.equals(this.authPassword, credential.authPassword) &&
        Objects.equals(this.privPassword, credential.privPassword);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userName, typeUserName, authPassword, privPassword);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Credential {\n");
    
    sb.append("    userName: ").append(toIndentedString(userName)).append("\n");
    sb.append("    typeUserName: ").append(toIndentedString(typeUserName)).append("\n");
    sb.append("    authPassword: ").append(toIndentedString(authPassword)).append("\n");
    sb.append("    privPassword: ").append(toIndentedString(privPassword)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
