package com.sibs.agent.dto;

public class Mib {

    public enum MibOid {
        STATUSPRT("1.3.6.1.4.1.8072.7777.1.1.0"),
        TOTALSESSION("1.3.6.1.4.1.8072.7777.1.2.0"),
        MESSAGE("1.3.6.1.4.1.8072.7777.1.3.0"),
        TRAPS("1.3.6.1.4.1.8072.7777.4"),
        STATUSCHANGE("1.3.6.1.4.1.8072.7777.4.1.0"),
        CHANGESTATUSSESSION("1.3.6.1.4.1.8072.7777.4.2.0"),
        TRAFFICSTATUSCHANGE("1.3.6.1.4.1.8072.7777.4.3.0"),
        TRANSPORTSTATUSCHANGE("1.3.6.1.4.1.8072.7777.4.4.0"),
        ILTUSED("1.3.6.1.4.1.8072.7777.4.5.0"),
        ILTREP("1.3.6.1.4.1.8072.7777.4.6.0"),
        ILTFREE("1.3.6.1.4.1.8072.7777.4.7.0");

        private final String value;

        MibOid(final String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }

        public static MibOid getEnum(String compare) {
            for( MibOid v : values() )
                if(v.value.equalsIgnoreCase(compare)) return v;
            throw new IllegalArgumentException();
        }

    }
}
