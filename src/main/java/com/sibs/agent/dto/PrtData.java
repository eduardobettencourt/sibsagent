package com.sibs.agent.dto;

import lombok.Data;

@Data
public class PrtData {
    private String session;
    private String stateSession;
    private String stateTraffic;
    private String stateTransport;
    private boolean update;

    public PrtData(String session, String stateSession, String stateTraffic, String stateTransport, boolean update) {
        this.session = session;
        this.stateSession = stateSession;
        this.stateTraffic = stateTraffic;
        this.stateTransport = stateTransport;
        this.update = update;
    }

    public PrtData() {

    }

}
