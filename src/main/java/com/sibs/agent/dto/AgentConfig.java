package com.sibs.agent.dto;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AgentConfig
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-01-24T15:16:39.713Z[GMT]")


public class AgentConfig   {
  @JsonProperty("host")
  private String host = null;

  @JsonProperty("port")
  private Integer port = null;

  public AgentConfig host(String host) {
    this.host = host;
    return this;
  }

  /**
   * Get host
   * @return host
   **/
  @Schema(description = "")
  
    public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public AgentConfig port(Integer port) {
    this.port = port;
    return this;
  }

  /**
   * Get port
   * @return port
   **/
  @Schema(description = "")
  
    public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AgentConfig agentConfig = (AgentConfig) o;
    return Objects.equals(this.host, agentConfig.host) &&
        Objects.equals(this.port, agentConfig.port);
  }

  @Override
  public int hashCode() {
    return Objects.hash(host, port);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AgentConfig {\n");
    
    sb.append("    host: ").append(toIndentedString(host)).append("\n");
    sb.append("    port: ").append(toIndentedString(port)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
