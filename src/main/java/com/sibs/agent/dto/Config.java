package com.sibs.agent.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Config
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-01-24T13:45:53.547Z[GMT]")


public class Config {
    @JsonProperty("description")
    private String description = null;

    @JsonProperty("poolTime")
    private Integer poolTime = null;

    @JsonProperty("prtServer")
    private PrtServer prtServer = null;

    @JsonProperty("agentConfig")
    private AgentConfig agentConfig = null;

    @JsonProperty("trapsConfig")
    @Valid
    private List<Traps> trapsConfig = null;

    public Config description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Get description
     *
     * @return description
     **/
    @Schema(description = "")

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Config poolTime(Integer poolTime) {
        this.poolTime = poolTime;
        return this;
    }

    /**
     * Get poolTime
     *
     * @return poolTime
     **/
    @Schema(description = "")

    public Integer getPoolTime() {
        return poolTime;
    }

    public void setPoolTime(Integer poolTime) {
        this.poolTime = poolTime;
    }

    public Config prtServer(PrtServer prtServer) {
        this.prtServer = prtServer;
        return this;
    }

    /**
     * Get prtServer
     *
     * @return prtServer
     **/
    @Schema(description = "")

    @Valid
    public PrtServer getPrtServer() {
        return prtServer;
    }

    public void setPrtServer(PrtServer prtServer) {
        this.prtServer = prtServer;
    }

    public Config agentConfig(AgentConfig agentConfig) {
        this.agentConfig = agentConfig;
        return this;
    }

    /**
     * Get agentConfig
     *
     * @return agentConfig
     **/
    @Schema(description = "")

    @Valid
    public AgentConfig getAgentConfig() {
        return agentConfig;
    }

    public void setAgentConfig(AgentConfig agentConfig) {
        this.agentConfig = agentConfig;
    }

    public Config trapsConfig(List<Traps> trapsConfig) {
        this.trapsConfig = trapsConfig;
        return this;
    }

    public Config addTrapsConfigItem(Traps trapsConfigItem) {
        if (this.trapsConfig == null) {
            this.trapsConfig = new ArrayList<Traps>();
        }
        this.trapsConfig.add(trapsConfigItem);
        return this;
    }

    /**
     * Get trapsConfig
     *
     * @return trapsConfig
     **/
    @Schema(description = "")
    @Valid
    public List<Traps> getTrapsConfig() {
        return trapsConfig;
    }

    public void setTrapsConfig(List<Traps> trapsConfig) {
        this.trapsConfig = trapsConfig;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Config config = (Config) o;
        return Objects.equals(this.description, config.description) &&
                Objects.equals(this.poolTime, config.poolTime) &&
                Objects.equals(this.prtServer, config.prtServer) &&
                Objects.equals(this.agentConfig, config.agentConfig) &&
                Objects.equals(this.trapsConfig, config.trapsConfig);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, poolTime, prtServer, agentConfig, trapsConfig);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Config {\n");

        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    poolTime: ").append(toIndentedString(poolTime)).append("\n");
        sb.append("    prtServer: ").append(toIndentedString(prtServer)).append("\n");
        sb.append("    agentConfig: ").append(toIndentedString(agentConfig)).append("\n");
        sb.append("    trapsConfig: ").append(toIndentedString(trapsConfig)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
