package com.sibs.agent.dto;

import lombok.Data;

@Data
public class Session {

    private String Prtses;
    private String typ;
    private String dgn;
    private String hdr;
    private String sta;
    private String trf;
    private String trp;
    private int Ilts;
    private int Rty;
    private int Fre;

}
