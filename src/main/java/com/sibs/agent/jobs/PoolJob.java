package com.sibs.agent.jobs;

import com.sibs.agent.service.SibsService;
import com.sibs.agent.utility.SibsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class PoolJob {

    @Autowired
    private SibsService sibsService;
    @Scheduled(fixedDelay =10)
    public void Begin(){
        SibsLog sibsLog = new SibsLog();

        try {
            Thread.sleep(sibsService.start());
        } catch (InterruptedException e) {
            sibsLog.log(SibsLog.SibsErroStatus.SEVERE, "#ERRO ON  PoolJob.Begin {0}#", new Object[]{e.getMessage()});
        }
    }
}
