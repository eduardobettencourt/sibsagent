package com.sibs.agent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SibsAgentApplication {

    public static void main(String[] args) {
        SpringApplication.run(SibsAgentApplication.class, args);
    }

}
