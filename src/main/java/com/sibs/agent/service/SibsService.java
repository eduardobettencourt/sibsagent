package com.sibs.agent.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sibs.agent.agent.AgentV3;
import com.sibs.agent.dto.*;
import com.sibs.agent.utility.SibsLog;
import org.snmp4j.agent.CommandProcessor;
import org.snmp4j.mp.MPv3;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;

@Service
public class SibsService {

    @Value("${sibs.agent.credentials}")
    private Resource resourceCredentials;

    @Value("${sibs.agent.server.config}")
    private Resource resourceServerConfig;

    private final SibsLog sibsLog = new SibsLog();

    private Config agentConfig;
    private Credentials credentials;
    private AgentV3 agentV3;

    private String statePrt = "";

    private final List<PrtData> prtDataList = new ArrayList<>();

    public int start() {

        ObjectMapper mapper = new ObjectMapper();

        try {
            credentials = mapper.readValue(resourceCredentials.getFile(), Credentials.class);
            agentConfig = mapper.readValue(resourceServerConfig.getFile(), Config.class);
            PrtService prtService = new PrtService();

            List<Session> sessionList = prtService.getSessions(agentConfig.getPrtServer().getHost(), agentConfig.getPrtServer().getPort());

            // List<Session> sessionList = prtService.getSessions();

            if (agentV3 == null && credentials != null && agentConfig != null) {
                agentV3 = new AgentV3(new CommandProcessor(new OctetString(MPv3.createLocalEngineID())));
                agentV3.setCredentials(credentials);
                agentV3.setAgentConfig(agentConfig);
                agentV3.start(agentConfig.getAgentConfig().getHost(), agentConfig.getAgentConfig().getPort());
            }

           List<VariableBinding[]> result = createTraps(sessionList);

            for (VariableBinding[] variableBindings: result){
                sendSnmpV3Trap(variableBindings);
            }


        } catch (IOException | ParseException e) {
            sibsLog.log(SibsLog.SibsErroStatus.SEVERE, "#ERRO ON  SibsService.start {0}#", new Object[]{e.getMessage()});
        }

        return agentConfig.getPoolTime();
    }

    private void sendSnmpV3Trap(VariableBinding[] variableBindings) throws ParseException {
        agentV3.sendnotification(new OctetString(), new OID(Mib.MibOid.TRAPS.toString()), variableBindings);
    }

    private List<VariableBinding[]> createTraps(List<Session> sessionList) {
        List<VariableBinding[]> retrnValues = new ArrayList<>();
        List<Map<String,String>> hashMaps = new ArrayList<Map<String,String>>();
        Map<String, String> values = new HashMap<>();

        if (sessionList.isEmpty()) {
            values.put(Mib.MibOid.STATUSCHANGE.toString(), "PRT DOWN");
            statePrt = "";

            return this.returnPrtDown();
        } else {
            if (statePrt.equals("")) {
                values.put(Mib.MibOid.STATUSCHANGE.toString(), "PRT UP");
            }
            statePrt = "PRT UP";

            for (Session session : sessionList) {

                if (findPrtData(session.getPrtses(), session.getSta(), null, null)) {
                    values.put(Mib.MibOid.CHANGESTATUSSESSION.toString(), session.getSta());
                }

                if (findPrtData(session.getPrtses(), null, session.getTrf(), null)) {
                    values.put(Mib.MibOid.TRAFFICSTATUSCHANGE.toString(), session.getTrf());
                }

                if (findPrtData(session.getPrtses(), null, null, session.getTrp())) {
                    values.put(Mib.MibOid.TRANSPORTSTATUSCHANGE.toString(), session.getTrp());
                }

                int iltsUsade = session.getIlts() - session.getFre();

                values.put(Mib.MibOid.ILTUSED.toString(), String.valueOf(iltsUsade));
                values.put(Mib.MibOid.ILTREP.toString(), String.valueOf(session.getRty()));
                values.put(Mib.MibOid.ILTFREE.toString(), String.valueOf(session.getFre()));
                hashMaps.add(values);
                values = new HashMap<>();
            }
        }

        this.updatePrtData();

        int count = 0;

        for (Map<String, String> key : hashMaps) {
            VariableBinding[] variableBindings = new VariableBinding[key.size()];
            for (Map.Entry<String, String> keyValues : key.entrySet()) {
                variableBindings[count] = new VariableBinding(new OID(keyValues.getKey()), new OctetString(keyValues.getValue()));
                count++;
            }
            count=0;
            retrnValues.add(variableBindings);
        }

        return retrnValues;
    }

    private List<VariableBinding[]> returnPrtDown(){
        List<VariableBinding[]> returnValues = new ArrayList<>();
        VariableBinding[] variableBindings = new VariableBinding[1];
        variableBindings[0] = new VariableBinding(new OID(Mib.MibOid.STATUSCHANGE.toString() ), new OctetString( "PRT DOWN"));
        returnValues.add(variableBindings);

        return  returnValues;
    }

    private void updatePrtData() {
       prtDataList.removeIf(t -> !t.isUpdate());
    }

    private boolean findPrtData(String session, String stateSession, String stateTraffic, String stateTransport) {
        boolean returnValue = true;
        PrtData findValue = null;

        ListIterator<PrtData> iterator = prtDataList.listIterator();
        while (iterator.hasNext()) {
            PrtData next = iterator.next();

            if (next.getSession().equals(session)) {
                findValue = next;

                if (stateSession != null) {
                    if (next.getStateSession() != null && next.getStateSession().equals(stateSession) && next.getSession().equals(session)) {
                        returnValue = false;
                    }
                } else if (stateTraffic != null) {
                    if (next.getStateTraffic() != null && next.getStateTraffic().equals(stateTraffic) && next.getSession().equals(session)) {
                        returnValue = false;
                    }
                } else if (stateTransport != null) {
                    if (next.getStateTransport() != null && next.getStateTransport().equals(stateTransport) && next.getSession().equals(session)) {
                        returnValue = false;
                    }
                }
                break;
            }
        }
        if (findValue == null) {
            PrtData prt = new PrtData();
            prt.setSession(session);
            prt.setUpdate(true);

            if (stateSession != null) {
                prt.setStateSession(stateSession);
            } else if (stateTraffic != null) {
                prt.setStateTraffic(stateTraffic);
            } else if (stateTransport != null) {
                prt.setStateTransport(stateTransport);
            }

            prtDataList.add(prt);
        } else {
            int index = prtDataList.indexOf(findValue);
            if (stateSession != null) {
                findValue.setStateSession(stateSession);
            } else if (stateTraffic != null) {
                findValue.setStateTraffic(stateTraffic);
            } else if (stateTransport != null) {
                findValue.setStateTransport(stateTransport);
            }

            prtDataList.set(index, findValue);
        }


        return returnValue;
    }

    public Config getServerConfig() {
        return agentConfig;
    }

    public void setServerConfig(Config serverConfig) {
        this.agentConfig = serverConfig;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }
}
