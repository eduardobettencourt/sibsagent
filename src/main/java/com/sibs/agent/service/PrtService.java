package com.sibs.agent.service;

import com.sibs.agent.dto.Session;
import com.sibs.agent.utility.SibsLog;
import sibs.deswin.lib.com.PersClientConnection;
import sibs.deswin.lib.com.SocketIO;

import java.net.ConnectException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class PrtService {

    private final SibsLog sibsLog = new SibsLog();

    public String execute(PersClientConnection clt) throws Exception {

        String returnValue = "";

        SocketIO _sio = null;
        clt.setParameter("", "");
        try {
            _sio = clt.connect(_sio);
            returnValue = _sio.readString();

        } catch (ConnectException e) {
            sibsLog.log(SibsLog.SibsErroStatus.SEVERE, "#ERRO ON  PrtService.execute {0}#", new Object[]{e.getMessage()});
            return null;
        } catch (Exception e) {
           throw  new Exception(e);
        } finally {
            if (null != _sio) {
                _sio.close();
                _sio = null;
            }
        }

        sibsLog.log(SibsLog.SibsErroStatus.INFO, "#SUCCESS PrtService.execute  ");

        return returnValue;
    }

    public List<Session> getSessions(String host, int port)  {


        return this.getSessionsTest();

       // return this.getSessionsPrd(host,port);
    }

    public List<Session> getSessionsPrd(String host, int port)  {

        List<Session> sessionList = new ArrayList<>();
        PersClientConnection clt = new PersClientConnection();
        clt.setHostName(host);
        clt.setHostPort(port);
        clt.setApplName("sibs.deswin.prt.PrtMonGatewaySnmp");

        try {

            String INPUT_STRING = execute(clt);
            Session session = new Session();
            String[] sessions = INPUT_STRING.split("\n");

            for (String s : sessions) {
                String[] parts = s.split("&");

                for (String c : parts) {
                    this.populateHeader(c, session);
                }

                sessionList.add(session);
                session = new Session();
            }

        } catch (Exception e) {
            sibsLog.log(SibsLog.SibsErroStatus.SEVERE, "#ERRO ON  PrtService.execute {0}#", new Object[]{e.getMessage()});
        }
        sibsLog.log(SibsLog.SibsErroStatus.INFO, "#Sucess  PrtService.execute get data. ");
        return sessionList;
    }

    public List<Session> getSessionsTest()  {

        List<Session> sessionList = new ArrayList<>();
        try {

            Path path = Paths.get("c:/temp/teste/snmp/teste.txt");

            List<String> linhasArquivo = Files.readAllLines(path);

            //String INPUT_STRING = linhasArquivo.get(0);

            Session session = new Session();
            //String[] sessions = INPUT_STRING.split("\n");

            for (String s : linhasArquivo) {
                String[] parts = s.split("&");

                for (String c : parts) {
                    this.populateHeader(c, session);
                }

                sessionList.add(session);
                session = new Session();
            }

        } catch (Exception e) {
            sibsLog.log(SibsLog.SibsErroStatus.SEVERE, "#ERRO ON  PrtService.execute {0}#", new Object[]{e.getMessage()});
        }

        return sessionList;
    }

    private void populateHeader(String header, Session session) throws Exception {

        String[] headerArray = header.split("=");
        try {
            switch (headerArray[0]) {
                case "PRTSES":
                    session.setPrtses(headerArray[1]);
                    break;
                case "TYP":
                    session.setTyp(headerArray[1]);
                    break;
                case "DGN":
                    session.setDgn(headerArray[1]);
                    break;
                case "HDR":
                    session.setHdr(headerArray[1]);
                    break;
                case "STA":
                    session.setSta(headerArray[1]);
                    break;
                case "TRF":
                    session.setTrf(headerArray[1]);
                    break;
                case "TRP":
                    session.setTrp(headerArray[1]);
                    break;
                case "ILTS":
                    session.setIlts(Integer.parseInt(headerArray[1]));
                    break;
                case "RTY":
                    session.setRty(Integer.parseInt(headerArray[1]));
                    break;
                case "FRE":
                    session.setFre(Integer.parseInt(headerArray[1]));
                    break;

            }
        } catch (Exception e) {
           throw new Exception(e);
        }

    }
}
