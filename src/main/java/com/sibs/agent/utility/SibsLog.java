package com.sibs.agent.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

public class SibsLog {

    public static final String ERRO = "INTERNAL ERRO";

    Logger LOGGER_ERRO = LoggerFactory.getLogger("com.sibs.snmp.erro");
    Logger LOGGER_INFO = LoggerFactory.getLogger("com.sibs.snmp.info");
    Logger LOGGER_WARN = LoggerFactory.getLogger("com.sibs.snmp.warn");
    Logger LOGGER_AGENT = LoggerFactory.getLogger("com.sibs.snmp.agent");

    public void log(SibsErroStatus level, String msg, Object... params) {
        this.switchLogType(level, MessageFormat.format(msg, params));
    }

    public void log(SibsErroStatus level, String msg) {
        this.switchLogType(level, msg);
    }

    private SibsErroStatus status;

    private void switchLogType(SibsErroStatus level, String message) {
        switch (level) {
            case SEVERE:
                LOGGER_ERRO.error(message);
                break;
            case INFO:
                LOGGER_INFO.info(message);
                break;
            case WARN:
                LOGGER_WARN.warn(message);
                break;
            case ALL:
                LOGGER_AGENT.warn(message);
                break;
        }
    }

    public enum SibsErroStatus {
        SEVERE,
        INFO,
        WARN,
        ALL;
    }
}
